<?php

/**
 * @file SubmissionReminder.inc.php
 *
 * @class SubmissionReminder
 * @ingroup plugins_generic_editorReminders
 *
 * @brief Class to perform automated reminders of stalled submissions to editors.
 */

import('lib.pkp.classes.scheduledTask.ScheduledTask');

class SubmissionReminder extends ScheduledTask {
	
	/**
	 * @copydoc ScheduledTask::getName()
	 */
	function getName() {
		return __('plugins.generic.editorReminders.scheduledTask.submissionReminder');
	}
	
	/**
	 * @copydoc ScheduledTask::executeActions()
	 */
	function executeActions() {
		$contextDao = Application::getContextDAO(); /* @var $contextDao JournalDAO */
		$submissionDao = Application::getSubmissionDAO(); /* @var $submissionDao ArticleDAO */

		$journalFactory = $contextDao->getAll();
		while($journal = $journalFactory->next()) { /* @var $journal Journal */
			$plugin = PluginRegistry::getPlugin('generic', 'editorremindersplugin');
			if (is_null($plugin) || !$plugin->getEnabled($journal->getId())) {
				// plugin not activated for journal, skip
				return;
			}

			$this->addExecutionLogEntry(
				__('plugins.generic.editorReminders.scanJournalLogEntry', array( 'acronym' => $journal->getAcronym($journal->getPrimaryLocale()) )),
				SCHEDULED_TASK_MESSAGE_TYPE_NOTICE);

			// scan submissions
			$submissionFactory = $submissionDao->getByStatus(STATUS_QUEUED, null, $journal->getId(), null, null, null, WORKFLOW_STAGE_ID_EXTERNAL_REVIEW, null, null, null);
			while($submission = $submissionFactory->next()) {
				$this->checkSubmission($submission);
			}
		}
		
		return true;
	}

	/**
	 * Check if a submission is stalled, requiring action from the editors.
	 *
	 * It tests the status of the submission and sends a reminder to the editors
	 * in charge if:
	 * * the submission is pending reviewers for a few days
	 * * the author has uploaded a revision or resubmitted a few days ago
	 *
	 * @param $submission Submission the submission to check
	 */
	private function checkSubmission($submission) {
		$reviewRoundDao = DAORegistry::getDAO('ReviewRoundDAO'); /* @var $reviewRoundDao ReviewRoundDAO */

		$submissionId = $submission->getId();

		$lastReviewRound = $reviewRoundDao->getLastReviewRoundBySubmissionId($submissionId);
		assert(isset($lastReviewRound));

		// filter by status of last review round
		$status = $lastReviewRound->getStatus();
		if (!in_array($status, array( REVIEW_ROUND_STATUS_PENDING_REVIEWERS, REVIEW_ROUND_STATUS_REVISIONS_SUBMITTED, REVIEW_ROUND_STATUS_RESUBMIT_FOR_REVIEW_SUBMITTED ))) {
			// no action required from editor(s), skip
			return;
		}

		// get the date of the last relevant action
		$date = null;
		switch($status) {
			case REVIEW_ROUND_STATUS_PENDING_REVIEWERS:
				// the date of the decision
				$editDecisionDao = DAORegistry::getDAO('EditDecisionDAO'); /* @var $editDecisionDao EditDecisionDAO */
				// the date of the pending decision
				$editorDecisions = $editDecisionDao->getEditorDecisions($submissionId);
				assert(isset($editorDecisions));
				$latestDecision = array_pop($editorDecisions);
				assert(array_key_exists('dateDecided', $latestDecision));
				$date = strtotime($latestDecision['dateDecided']);
				break;

			case REVIEW_ROUND_STATUS_REVISIONS_SUBMITTED:
			case REVIEW_ROUND_STATUS_RESUBMIT_FOR_REVIEW_SUBMITTED:
				// the date of the last revision
				$submissionFileDao = DAORegistry::getDAO('SubmissionFileDAO'); /* @var $submissionFileDao SubmissionFileDAO */
				import('lib.pkp.classes.submission.SubmissionFile'); // Bring the file constants.
				$submissionFiles = $submissionFileDao->getRevisionsByReviewRound($lastReviewRound, SUBMISSION_FILE_REVIEW_REVISION);
				assert(is_array($submissionFiles));
				// find the date of the latest change to revisions
				foreach($submissionFiles as $file) {
					$uploaded = strtotime($file->getDateUploaded());
					if (is_null($date))
						$date = $uploaded;
					else
						$date = max($date, $uploaded);
				}
				break;
		}
		assert(isset($date));

		$timeStalled = time() - $date;
		$daysStalled = round($timeStalled / (60 * 60 * 24));
		$decisionLeeway = 2; // min days before reminder
		if ($daysStalled < $decisionLeeway) {
			// skip submission with fresh decision
			return;
		}

		// select the reminder/log entry based on the status of the current round
		$emailKey = $messageKey = null;
		switch ($status) {
			// Waiting for reviewers to be assigned
			case REVIEW_ROUND_STATUS_PENDING_REVIEWERS:
				$emailKey = 'EDITOR_REMINDERS_PENDING_REVIEWERS';
				$messageKey = 'plugins.generic.editorReminders.pendingReviewersLogEntry';
				break;

			// Revisions have been submitted
			case REVIEW_ROUND_STATUS_REVISIONS_SUBMITTED:
			// Submission has been resubmitted for another review round
			case REVIEW_ROUND_STATUS_RESUBMIT_FOR_REVIEW_SUBMITTED:
				$emailKey = 'EDITOR_REMINDERS_REVISIONS_SUBMITTED';
				$messageKey = 'plugins.generic.editorReminders.revisionsSubmittedLogEntry';
				break;
		}

		$this->addExecutionLogEntry(
			__($messageKey, array('submissionId' => $submissionId, 'status' => $status, 'lag' => $daysStalled)),
			SCHEDULED_TASK_MESSAGE_TYPE_NOTICE);
		$this->sendReminder($submission, $emailKey);
	}

	/**
	 * Send an email to the editors in charge of a stalled submission.
	 * 
	 * @param $submission Submission the submission
	 * @param $emailKey string the mail template identifier
	 */
	private function sendReminder($submission, $emailKey) {
		$contextDao = Application::getContextDAO(); /* @var $contextDao JournalDAO */
		$stageAssignmentDao = DAORegistry::getDAO('StageAssignmentDAO'); /* @var $stageAssignmentsDao StageAssignmentDAO */
		$userDao = DAORegistry::getDAO('UserDAO'); /* @var $userDao UserDAO */

		$context = $contextDao->getById($submission->getContextId());

		import('lib.pkp.classes.mail.SubmissionMailTemplate');
		$email = new SubmissionMailTemplate($submission, $emailKey, $context->getPrimaryLocale(), $context, false);
		$email->setContext($context); // required
		$email->setReplyTo(null);
		
		$editorStageAssignments = $stageAssignmentDao->getEditorsAssignedToStage($submission->getId(), WORKFLOW_STAGE_ID_EXTERNAL_REVIEW);
		foreach ($editorStageAssignments as $editorStageAssignment) { /* @var $editorStageAssignment StageAssignment */
			$userId = $editorStageAssignment->getUserId();
			$email->addRecipient($userDao->getUserEmail($userId), $userDao->getUserFullName($userId));
		}

		$email->setSubject($email->getSubject($context->getPrimaryLocale()));
		$email->setBody($email->getBody($context->getPrimaryLocale()));
		$email->setFrom($context->getSetting('contactEmail'), $context->getSetting('contactName'));

		$application = PKPApplication::getApplication();
		$request = $application->getRequest();
		$dispatcher = $application->getDispatcher();

		$paramArray = array(
		    'submissionUrl' => $dispatcher->url($request, ROUTE_PAGE, $context->getPath(), 'workflow', 'index', array($submission->getId(), WORKFLOW_STAGE_ID_EXTERNAL_REVIEW)),
		);
		$email->assignParams($paramArray);

		$email->send();
	}
}

?>