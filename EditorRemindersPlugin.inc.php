<?php

/**
 * @file EditorRemindersPlugin.inc.php
 *
 * @class EditorRemindersPlugin
 * @ingroup plugins_generic_editorReminders
 *
 * @brief Plugin checking for stalled submissions and reminding editors.
 */

import('lib.pkp.classes.plugins.GenericPlugin');

class EditorRemindersPlugin extends GenericPlugin {

	//
	// Implement methods from Plugin
	//
	/**
	 * @copydoc Plugin::register()
	 */
	function register($category, $path, $mainContextId = null) {
		if (parent::register($category, $path, $mainContextId)) {
			if ($this->getEnabled($mainContextId)) {
				// TODO plugin config
			}
			return true;
		}
		return false;
	}

	/**
	 * @copydoc Plugin::getDescription()
	 */
	public function getDescription() {
		return __('plugins.generic.editorReminders.description');
	}

	/**
	 * @copydoc Plugin::getDisplayName()
	 */
	public function getDisplayName() {
		return __('plugins.generic.editorReminders.displayName');
	}
	
	/**
	 * @copydoc PKPPlugin::getInstallEmailTemplatesFile()
	 */
	function getInstallEmailTemplatesFile() {
		return ($this->getPluginPath() . '/emailTemplates.xml');
	}

	/**
	 * @copydoc PKPPlugin::getInstallEmailTemplateDataFile()
	 */
	function getInstallEmailTemplateDataFile() {
		return ($this->getPluginPath() . '/locale/{$installedLocale}/emailTemplates.xml');
	}
}
