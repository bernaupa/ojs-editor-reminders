<?php

/**
 * @defgroup plugins_generic_editorReminders Editor Reminders plugin
 */

/**
 * @file index.php
 *
 * @ingroup plugins_generic_editorReminders
 * @brief Wrapper for Editor Reminders plugin.
 *
 */
require_once('EditorRemindersPlugin.inc.php');

return new EditorRemindersPlugin();
