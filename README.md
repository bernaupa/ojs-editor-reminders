# Editor Reminders plugin for OJS

## About

This plugin scans the queued submissions of a journal and reports any stalled submission to the responsible editors.

The editors are reminded by email when:
 - a submission has no assigned reviewer
 - the authors of a submission have uploaded a revised version


## System requirements

This plugin has been tested on OJS version 3.1.1-4 with [PR #4605](https://github.com/pkp/pkp-lib/pull/4605) applied.


## Installation

To install:
 - unpack the plugin archive to OJS's `plugin/generic` directory;
 - enable the plugin by ticking the checkbox for "Editor Reminders" in the set of generic plugins available ('Settings' > 'Website' > 'Plugins' > 'Generic Plugins').

**Note**:
Make sure the name of the directory for the plugin is 'editorReminders', not 'ojs-editor-reminders'.

To run the task (from the OJS directory):

    $ php tools/runScheduledTasks.php plugins/generic/editorReminders/scheduledTasks.xml
